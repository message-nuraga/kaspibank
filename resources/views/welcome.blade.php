<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Система управление фермой</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="/assets/css/main.css" rel="stylesheet" type="text/css">

    </head>
    <body>
    <div class="container">
        <h2>Фермочка для овечок</h2>
        <div class="row">
            @for($i=0;$i<4;$i++)
            <div class="col-md-6">
                <h4 >Загон {{$i+1}}</h4>
                    @if(Helper::getCheck($rows['random'],$i+1))
                        <ul class="corral selectable " id="corral_{{$i+1}}" data-corral="{{count($rows['random'][$i+1])}}" >
                            @foreach($rows['random'][$i+1] as $item)
                                    <li class="ui-widget-content" onclick="onDelete({{$item->id}});" id="content_{{$item->id}}">{{$item->title_ru}}</li>
                            @endforeach
                        </ul>
                    @else
                        <ul class="corral selectable ">
                            <li >Загон пустой</li>
                        </ul>
                    @endif
            </div>
            @endfor
        </div>
        <a href=""><button type="button" class="btn btn-success">Старт</button></a>
        <p>Чтобы зарубить овечок кликайте по нему</p>
        <p>Уровен Маниакальности : <span id="mani">0</span>%</p>
    </div>
        <script src="/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script>
            var c = 1;
            setInterval(function(){
                var corral_1 = $('#corral_1').attr('data-corral') ? $('#corral_1').attr('data-corral') : 0;
                var corral_2 = $('#corral_2').attr('data-corral') ? $('#corral_2').attr('data-corral'): 0;
                var corral_3 = $('#corral_3').attr('data-corral') ? $('#corral_3').attr('data-corral') : 0;
                var corral_4 = $('#corral_4').attr('data-corral') ? $('#corral_4').attr('data-corral') : 0;
                var corrals = [corral_1,corral_2,corral_3,corral_4];
                $.ajax({
                    url: "/add-sheep",
                    type:'POST',
                    data : {
                        _token : '{{csrf_token()}}',
                        corrals : corrals,
                        counter : c
                    },
                    error: function(){

                    },
                    success: function(data){
                        var json = $.parseJSON(data);
                        for (var i=0;i< Object.keys(json).length;i++){
                            var element = json[Object.keys(json)[i]];
                            var data_corral = parseInt($('#corral_'+element.corral).attr('data-corral'))+c;
                            $('#corral_'+element.corral).attr('data-corral',data_corral);
                            $('#corral_'+element.corral).append('<li class="ui-widget-content" onclick="onDelete('+element.id+')" id="content_'+element.id+'" >Овечка'+element.id+'</li>');
                        }
                    }
                });
            },3000);
        </script>
        <script>
            function onDelete(number) {
                var id =number;
                var c = 1;
                $.ajax({
                    url: "/delete-sheep",
                    type:'POST',
                    data : {
                        _token : '{{csrf_token()}}',
                        id : id
                    },
                    error: function(){

                    },
                    success: function(data){
                        var corral = parseInt($('#content_'+id).parents().attr('data-corral'));
                        $('#content_'+id).parents().attr('data-corral',corral-c);
                        $('#content_'+data.id).hide();
                        $('#mani').html(data.mani);
                    }
                });
            };
        </script>
    </body>
</html>
