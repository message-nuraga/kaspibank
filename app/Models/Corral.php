<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corral extends Model
{
    protected $table = 'corrals';

    protected $guarded = ['id'];
}
