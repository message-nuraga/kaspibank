<?php

namespace App\Http\Controllers;

use App\Models\Corral;
use App\Models\Sheep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function getRandom()
    {
        $this->resetSheepCorral();
        $this->insertData(10);
        $rows ['corrals'] = Corral::get();
        $rows['sheeps'] = Sheep::where('type',1)->get();
        $json_encode = json_encode($rows['sheeps']);
        $json_decode = json_decode($json_encode);

        foreach ($json_decode as $item) {
            $rows['random'][rand(1,count($rows ['corrals']))][] =$item;
        }

        foreach ($rows['random'] as $key=> $row) {
            foreach ($row as $item) {
                DB::table('sheep_corral')->insert([
                    'corral_id'=>$key,
                    'sheep_id'=>$item->id
                ]);
            }
        }
        return view('welcome',compact('rows'));
    }

    public function resetSheepCorral()
    {
        DB::table('sheep_corral')->where('sheep_id','>',0)->delete();
        Sheep::where('type','>=',0)->delete();
    }

    public function postAddSheep( Request $request)
    {
        $corrals = $request['corrals'];
        $counter = $request['counter'];
        $result = [];
        $j=0;
        foreach ($corrals as $key => $corral) {
            if ($corral > 1){
                for ($i=0;$i<$counter;$i++){
                    $number = rand(10,100000);
                    $sheep = new Sheep();
                    $sheep['title_ru'] = 'Овечка '.$number;
                    $sheep['title_en'] = 'Sheep '.$number;
                    $sheep['type'] = 0;
                    $sheep->save();

                    $result[$key+1]['corral'] = $key+1;
                    $result[$key+1]['id'] = $sheep['id'];
                    $result[$key+1]['title_ru'] = 'Овечка '.$number;
                    $result[$key+1]['title_en'] = 'Sheep '.$number;

                    DB::table('sheep_corral')->insert([
                        'sheep_id'=>$sheep['id'],
                        'corral_id'=>$key+1
                    ]);
                }
            }
        }
        return json_encode($result);
    }


    public function postDeleteSheep(Request $request)
    {
        $id = $request['id'];
        $count = Sheep::where('id','>',0)->count();
        $mani = 100-(100*($count-1))/$count; // Здес алгоритм не правильно проста времени не было
        Sheep::where('id',$id)->delete();
        DB::table('sheep_corral')->where('sheep_id',$id)->delete();
        $result['id'] = $id;
        $result['mani'] = $mani;
        return $result;
    }

    private function insertData($count){

        for ($i=0;$i<$count;$i++){
            $sheep = new Sheep();
            $sheep['title_ru'] = 'Овечка '.$i;
            $sheep['title_en'] = 'Sheep '.$i;
            $sheep['type'] = '1';
            $sheep->save();
        }
        return true;
    }
}
