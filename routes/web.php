<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::get('/', 'IndexController@getRandom');
Route::post('/add-sheep', 'IndexController@postAddSheep');
Route::post('/delete-sheep', 'IndexController@postDeleteSheep');
